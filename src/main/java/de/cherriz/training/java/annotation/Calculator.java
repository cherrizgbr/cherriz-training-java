package de.cherriz.training.java.annotation;

public interface Calculator<T extends Number> {

    T add(T valA, T valB);

    T remove(T valA, T valB);

    T multiply(T valA, T valB);

    T divide(T valA, T valB);

    @NotYetImplemented
    T getNextPrime(T start);

}