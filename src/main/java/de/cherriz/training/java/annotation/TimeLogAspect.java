package de.cherriz.training.java.annotation;

import net.sf.aspect4log.Log;
import net.sf.aspect4log.slf4j.LoggerFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;

@Aspect
public class TimeLogAspect {

    private Map<Class<?>, Logger> logCache = new HashMap<>();

    @Around("(execution(@de.cherriz.training.java.annotation.TimeLog *.new(..)) || execution(@de.cherriz.training.java.annotation.TimeLog * *.*(..)) ) && @annotation(timeLog)")
    public Object timeLogAnnotatedMethod(ProceedingJoinPoint joinPoint, TimeLog timeLog) throws Throwable {
        /* Aufruf mit Messung */
        long start = System.currentTimeMillis();
        Object result = joinPoint.proceed();

        /* LogMessage erzeugen */
        StringBuilder sb = createLogMessage(joinPoint, System.currentTimeMillis() - start, timeLog.info());

        /* Logeintrag schreiben */
        log(getLoggerForClass(joinPoint.getTarget().getClass()), timeLog.level(), sb);

        /* Ergebnis Methodenaufruf zurueckgeben */
        return result;
    }

    private Logger getLoggerForClass(Class<?> cls) {
        if (!logCache.containsKey(cls)) {
            logCache.put(cls, LoggerFactory.getLogger(cls));
        }
        return logCache.get(cls);
    }

    private StringBuilder createLogMessage(JoinPoint joinPoint, long duration, String info) {
        StringBuilder sb = new StringBuilder("time ");

        /* Methodenname */
        boolean isConstractorCall = "constructor-execution".equals(joinPoint.getStaticPart().getKind());
        String methodName = joinPoint.getSignature().getName();
        if (isConstractorCall) {
            methodName = joinPoint.getSignature().getDeclaringType().getSimpleName();
        }
        sb.append(methodName).append(" : ").append(duration);

        /* Info */
        if (info.length() > 0) {
            sb.append(" (").append(info).append(")");
        }

        return sb;
    }

    private void log(Logger logger, Log.Level level, StringBuilder builder) {
        switch (level) {
            case TRACE:
                if (logger.isTraceEnabled()) {
                    logger.trace(builder.toString());
                }
                break;
            case DEBUG:
                if (logger.isDebugEnabled()) {
                    logger.debug(builder.toString());
                }
                break;
            case INFO:
                if (logger.isInfoEnabled()) {
                    logger.info(builder.toString());
                }
                break;
            case WARN:
                if (logger.isWarnEnabled()) {
                    logger.warn(builder.toString());
                }
                break;
            case ERROR:
                if (logger.isErrorEnabled()) {
                    logger.error(builder.toString());
                }
                break;
            default:
        }
    }

}