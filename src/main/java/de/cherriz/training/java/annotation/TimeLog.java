package de.cherriz.training.java.annotation;

import net.sf.aspect4log.Log;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface TimeLog {

    String info() default "";

    Log.Level level() default Log.Level.DEBUG;

}