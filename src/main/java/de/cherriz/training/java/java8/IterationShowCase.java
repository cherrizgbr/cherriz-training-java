package de.cherriz.training.java.java8;

import java.util.List;
import java.util.function.Consumer;

public class IterationShowCase {

    public void iteratePriorJava5(List<String> liste) {
        for (int i = 0; i < liste.size(); i++) {
            System.out.println(liste.get(i));
        }
    }

    public void iterateJava5(List<String> liste) {
        for (String element : liste) {
            System.out.println(element);
        }
    }

    public void iterateJava8(List<String> liste) {
        liste.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

        liste.forEach((String s) -> {
            System.out.println(s);
        });

        liste.forEach(s -> System.out.println(s));

        liste.forEach(System.out::println);

        liste.forEach(this::print);
    }

    private void print(String s) {
        System.out.println(s);
    }

}