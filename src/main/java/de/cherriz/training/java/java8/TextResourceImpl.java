package de.cherriz.training.java.java8;

import java.util.HashMap;
import java.util.Map;

public class TextResourceImpl implements TextResource {

    private Map<Long, String> resources = new HashMap<>();

    public TextResourceImpl() {
        resources.put(1L, "Settings");
        resources.put(2L, "Load");
        resources.put(3L, "Save");
        resources.put(4L, "Quit");
    }

    @Override
    public String getRessourceFor(Long id) {
        return resources.get(id);
    }

}