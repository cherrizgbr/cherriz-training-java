package de.cherriz.training.java.java8;

public class BusinessCalculator {

    double price = 0;

    public void add(double prodPrice, double amount) {
        price += prodPrice * amount;
    }

    public void applyRule(BusinessOperation operation) {
        price = operation.perform(price);
    }

    public double getPrice() {
        return Math.round(100.0 * price) / 100.0;
    }

}