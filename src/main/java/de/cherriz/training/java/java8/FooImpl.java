package de.cherriz.training.java.java8;

public class FooImpl implements InterfaceA, InterfaceB{

    @Override
    public void foo() {
        InterfaceA.super.foo();
    }

}