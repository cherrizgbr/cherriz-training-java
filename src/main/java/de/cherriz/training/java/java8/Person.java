package de.cherriz.training.java.java8;

public class Person {

    private String vorname;

    private String nachname;

    private boolean male;

    private int alter;

    private String plz;

    public Person(String vorname, String nachname, boolean male, int alter, String plz){
        this.vorname = vorname;
        this.nachname = nachname;
        this.male = male;
        this.alter = alter;
        this.plz = plz;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public int getAlter() {
        return alter;
    }

    public String getPlz() {
        return plz;
    }

    public boolean isMale(){
        return male;
    }

}