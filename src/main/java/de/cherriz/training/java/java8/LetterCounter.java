package de.cherriz.training.java.java8;

import java.util.List;
import java.util.Optional;

public class LetterCounter {

    public Optional<Integer> count(List<String> stringList) {
        return stringList.stream().map(String::toLowerCase)
                .filter(s -> !s.matches(".*[äüöß].*"))
                .map(String::length)
                .reduce((i1, i2) -> i1 + i2);
    }
}