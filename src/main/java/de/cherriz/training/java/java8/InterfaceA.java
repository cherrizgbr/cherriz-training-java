package de.cherriz.training.java.java8;

public interface InterfaceA {

    default void foo(){
        System.out.println("Interface A");
    }

}