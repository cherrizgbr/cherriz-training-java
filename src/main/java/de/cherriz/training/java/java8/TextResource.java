package de.cherriz.training.java.java8;

public interface TextResource {

    String enEN = "EN";

    default String getLanguage(){
        return enEN;
    }

    String getRessourceFor(Long id);

}