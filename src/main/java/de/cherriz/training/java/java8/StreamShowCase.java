package de.cherriz.training.java.java8;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

public class StreamShowCase {

    public OptionalDouble avgAge(List<Person> personen) {
        return personen.stream().mapToInt(Person::getAlter).average();
    }

    public long population(List<Person> persons, String zip) {
        return persons.stream().filter(p -> p.getPlz().equals(zip)).count();
    }

    public Optional<String> createGaesteliste(List<Person> persons, String zip) {
        return persons.stream().filter(p -> p.getPlz().equals(zip))
                .sorted((p1, p2) -> p1.getNachname().compareTo(p2.getNachname()))
                .map(this::createAnrede)
                .reduce((s1, s2) -> s1 + "\n" + s2);
    }

    private String createAnrede(Person person) {
        if (person.isMale()) {
            return "Herr " + person.getVorname() + " " + person.getNachname();
        }
        return "Frau " + person.getVorname() + " " + person.getNachname();
    }

}