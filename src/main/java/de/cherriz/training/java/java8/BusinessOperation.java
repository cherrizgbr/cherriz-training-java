package de.cherriz.training.java.java8;

@FunctionalInterface
interface BusinessOperation {

    double perform(double value);

}