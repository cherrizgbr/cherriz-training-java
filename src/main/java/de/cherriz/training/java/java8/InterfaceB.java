package de.cherriz.training.java.java8;

public interface InterfaceB {

    default void foo() {
        System.out.println("InterfaceB");
    }

}