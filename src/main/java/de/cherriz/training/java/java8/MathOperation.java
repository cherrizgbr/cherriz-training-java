package de.cherriz.training.java.java8;

@FunctionalInterface
interface MathOperation {

    int operation(int a, int b);

}