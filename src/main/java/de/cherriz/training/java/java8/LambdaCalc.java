package de.cherriz.training.java.java8;

public class LambdaCalc {

    public int calculate(int a, int b, MathOperation op) {
        return op.operation(a, b);
    }

}