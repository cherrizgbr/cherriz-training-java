package de.cherriz.training.java.reflection;

import java.util.LinkedList;
import java.util.List;

public class Fridge {

    private int temperature;

    private boolean open;

    private List<Object> stuff;

    public Fridge(int temperature) {
        this.temperature = temperature;
        this.open = false;
        this.stuff = new LinkedList<>();
    }

    public void open() {
        this.open = true;
    }

    public void close() {
        this.open = false;
    }

    public void addItem(Object item) {
        if (this.open) {
            this.stuff.add(item);
        } else {
            System.out.println(item + " liegt jetzt auf dem Boden.");
        }
    }

    @Override
    public String toString() {
        return LogUtil.getObjectValues(this);
    }

}