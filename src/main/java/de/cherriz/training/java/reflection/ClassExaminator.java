package de.cherriz.training.java.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ClassExaminator {

    public void listAttributes(Class<?> objClass) {
        Field[] objFields = objClass.getDeclaredFields();
        System.out.println("Analyseiere Klasse " + objClass.getCanonicalName());
        System.out.println("Attribute");
        for (Field field : objFields) {
            System.out.println("\t" + Modifier.toString(field.getModifiers()) + " " + field.getName() + " " + field.getType());
        }
    }

    public void listFunctions(Class<?> objClass) {
        Method[] objMethods = objClass.getDeclaredMethods();
        System.out.println("Analyseiere Klasse " + objClass.getCanonicalName());
        System.out.println("Funktionen");
        for (Method method : objMethods) {
            Class[] types = method.getParameterTypes();
            String typeString = "";
            for (Class type : types) {
                typeString += " " + type.getName();
            }
            System.out.println("\t" + Modifier.toString(method.getModifiers()) + " " + method.getName() + "(" + typeString + ")");
        }
    }

    public void listConstructors(Class<?> objClass) {
        Constructor<?>[] objConstructors = objClass.getDeclaredConstructors();
        System.out.println("Analyseiere Klasse " + objClass.getCanonicalName());
        System.out.println("Konstruktoren");
        for (Constructor constructor : objConstructors) {
            Class[] types = constructor.getParameterTypes();
            String typeString = "";
            for (Class type : types) {
                typeString += " " + type.getName();
            }
            System.out.println("\t" + Modifier.toString(constructor.getModifiers()) + " " + objClass.getName() + "(" + typeString + ")");
        }
    }

}