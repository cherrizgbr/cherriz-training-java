package de.cherriz.training.java.reflection;

import de.cherriz.training.java.generics.Calculator;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class CalculatorCreator {

    public static final String CONFIG = "calculator.properties";

    private String getImpl() {
        try {
            InputStream resource = this.getClass().getClassLoader().getResourceAsStream(CONFIG);
            Properties prop = new Properties();
            prop.load(resource);
            String calcImpl = prop.getProperty("calculator");
            resource.close();
            return calcImpl;
        } catch (IOException e) {
            System.err.println("Fehler beim Laden der Properties.");
        }
        return null;
    }

    public <T extends Number> Calculator<T> createCalculator() {
        try {
            Class<?> classImpl = Class.forName(getImpl());
            return (Calculator<T>) classImpl.getConstructors()[0].newInstance();
        } catch (ClassNotFoundException e) {
            System.err.println("Klasse existiert nicht.");
        } catch (InvocationTargetException e) {
            System.err.println("Fehler im aufgerufenen Objekt.");
        } catch (InstantiationException e) {
            System.err.println("Fehler bei der Instanziierung.");
        } catch (IllegalAccessException e) {
            System.err.println("Zugriff nicht erlaubt.");
        }
        return null;
    }

}