package de.cherriz.training.java.reflection;


public class Apple extends Fruit{

    private String type;

    private String region;

    public Apple(String type, String region){
        super(200);
        this.type = type;
        this.region = region;
    }

    @Override
    public String toString() {
        return LogUtil.getObjectValues(this);
    }

}