package de.cherriz.training.java.reflection;

public class Tresor {

    private int geld;

    private static final String CODE = "ABCD";

    private boolean open = false;

    public Tresor(int geld) {
        this.geld = geld;
    }

    public boolean open(String code) {
        if (CODE.equals(code)) {
            open = true;
            return true;
        }
        return false;
    }

    public int getMoney() {
        if (open) {
            int returnVal = geld;
            geld = 0;
            return returnVal;
        }
        throw new IllegalStateException("Der Tresor ist verschlossen.");
    }

}