package de.cherriz.training.java.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class LogUtil {

    /**
     * Erzeugt einen String der den aktuellen Inhalt des übergebenen Objekts repräsentiert.
     *
     * @param obj Das auszulesende Objekt.
     * @return Einen String welcher den aktuellen Zustand des Objekts repräsentiert.
     */
    public static String getObjectValues(Object obj) {
        Class<?> c = obj.getClass();

        // Objektinformationen
        StringBuilder result = new StringBuilder(c.getName());
        result.append("@");
        result.append(Integer.toHexString(obj.hashCode()));
        result.append(" [\n");

        // Werte auslesen
        result.append(LogUtil.listAttributes(c, obj));

        result.append("]");

        return result.toString();
    }

    /**
     * Die Funktion listet alle Attribute einer Klasse auf. Betrachtet werden dabei auch alle
     * Superklassen.
     *
     * @param c   Die Klasse deren Attribute ausgelesen werden sollen.
     * @param obj Das Objekt aus dem die Werte ausgelesen werden sollen.
     * @return Ein StringBuilder mit allen enthaltenen Werten.
     */
    private static StringBuilder listAttributes(Class<?> c, Object obj) {
        StringBuilder result = null;
        Class<?> superclass = c.getSuperclass();

        if (superclass != null) {
            result = LogUtil.listAttributes(superclass, obj);
            if (result.length() > 0) {
                result.append("\n");
            }
        } else {
            result = new StringBuilder();
        }

        // Einzelne Attribute aufbereiten
        for (int i = 0; i < c.getDeclaredFields().length; i++) {
            Field field = c.getDeclaredFields()[i];
            // Konstanten werden nicht ausgegeben
            if (Modifier.isFinal(field.getModifiers())) {
                continue;
            }
            // JPA Felder ignorieren
            if (field.getName().startsWith("_persistence_")) {
                continue;
            }

            // Typ, Name und Inhalt auslesen
            String fieldType = field.getType().getSimpleName();
            String fieldName = field.getName();
            String fieldValue = "****";

            try {
                if (field.getAnnotation(HideField.class) == null) {
                    // Schreibschutz für private Variablen wird temporär aufgehoben
                    boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    fieldValue = field.get(obj).toString().replace("\t", "\t\t");
                    field.setAccessible(accessible);
                }
            } catch (Exception e) {
                fieldValue = e.getMessage();
            }

            // Feld ausgeben
            result.append("\t");
            result.append(fieldType);
            result.append(" ");
            result.append(fieldName);
            result.append(": ");
            result.append(fieldValue);
            if (i + 1 < c.getDeclaredFields().length) {
                result.append("\n");
            }
        }

        return result;
    }

}