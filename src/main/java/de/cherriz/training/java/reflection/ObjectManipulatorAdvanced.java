package de.cherriz.training.java.reflection;

import java.lang.reflect.Field;

public class ObjectManipulatorAdvanced extends ObjectManipulator{

    public <T> T setFieldValue(Object object, String fieldName, T value) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            Object oldValue = field.get(object);
            field.set(object, value);
            return (T) oldValue ;
        } catch (NoSuchFieldException e) {
            System.err.println("Das Feld existiert nicht.");
        } catch (IllegalAccessException e) {
            System.err.println("Fehler beim Zugriff.");
        }
        return null;
    }

}