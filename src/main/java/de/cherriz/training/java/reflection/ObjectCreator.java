package de.cherriz.training.java.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ObjectCreator {

    public <T> T createSimpleObject(Class<T> classObj) {
        Object[] parameter = new Object[0];
        return createObjectWithAttributes(classObj, parameter);
    }

    public <T, P> T createObjectWithAttributes(Class<T> classObj, P... parameter) {
        Class<?>[] parameterTypes = new Class<?>[parameter.length];
        for (int i = 0; i < parameter.length; i++) {
            parameterTypes[i] = parameter[i].getClass();
        }
        T tObj = null;
        try {
            Constructor<T> constructor = classObj.getConstructor(parameterTypes);
            tObj = constructor.newInstance(parameter);
        } catch (NoSuchMethodException e) {
            System.err.println("Konstruktor existiert nicht.");
        } catch (IllegalAccessException e) {
            System.err.println("Konstruktor ist private.");
        } catch (InvocationTargetException e) {
            System.err.println("Fehler beim Aufruf der generischen Funktion.");
        } catch (InstantiationException e) {
            System.err.println("Objekt konnte nicht erzeugt werden.");
        }
        return tObj;
    }

    public <P> Object createObjectWithAttributesFromString(String className, P... parameter) {
        try {
            Class<?> classObj = Class.forName(className);
            return createObjectWithAttributes(classObj, parameter);
        } catch (ClassNotFoundException e) {
            System.err.println("Eine Klasse mit diesem namen existiert nicht.");
        }
        return null;
    }

}