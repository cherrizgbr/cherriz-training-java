package de.cherriz.training.java.reflection;

import java.time.LocalDate;

public class Milk {

    private int amount;

    private LocalDate expires;

    public Milk(int amount, LocalDate expires) {
        this.amount = amount;
        this.expires = expires;
    }

    @Override
    public String toString() {
        return LogUtil.getObjectValues(this);
    }

}