package de.cherriz.training.java.reflection;

public abstract class Fruit {

    private int weight;

    protected Fruit(int weight){
        this.weight = weight;
    }

}