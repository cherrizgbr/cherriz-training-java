package de.cherriz.training.java.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ObjectManipulator {

    public <T> T getFieldValue(Object object, String fieldName) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            Object value = field.get(object);
            return (T) value;
        } catch (NoSuchFieldException e) {
            System.err.println("Das Feld existiert nicht.");
        } catch (IllegalAccessException e) {
            System.err.println("Fehler beim Zugriff.");
        }
        return null;
    }

    public <P, R> R invokeMethod(Object object, String methodName, P... parameter) {
        Class<?>[] parameterTypes = new Class<?>[parameter.length];
        for (int i = 0; i < parameter.length; i++) {
            parameterTypes[i] = parameter[i].getClass();
        }
        try {
            Method method = object.getClass().getMethod(methodName, parameterTypes);
            return (R) method.invoke(object, parameter);
        } catch (NoSuchMethodException e) {
            System.err.println("Die Methode existiert nicht.");
        } catch (InvocationTargetException e) {
            System.err.println("Fehler im aufgerufenen Objekt.");
        } catch (IllegalAccessException e) {
            System.err.println("Fehler beim Zugriff.");
        }
        return null;
    }

}