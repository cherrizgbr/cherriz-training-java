package de.cherriz.training.java.reflection;

public class Cheese {

    private String type;

    private int slices;

    public Cheese(String type, int slices) {
        this.type = type;
        this.slices = slices;
    }

    @Override
    public String toString() {
        return LogUtil.getObjectValues(this);
    }

}