package de.cherriz.training.java.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericClassShowCase<T extends Number> {

    public List<T> createList(T item) {
        ArrayList<T> lst = new ArrayList<>();
        lst.add(item);
        return lst;
    }

}