package de.cherriz.training.java.generics;

/**
 * Showcase mehrere Rueckgabewerte in Java via Wrapper
 * @author Frederik Kirsch
 */
public class ResultShowCase {

    /**
     * Verdoppel de Wert wenn gerade.
     *
     * @param number Number to double.
     * @return Der verdoppelte Wert oder {@link Result#isEven()}.
     */
    public Result doubleIfEven(int number) {
        if (number % 2 == 0) {
            return new Result(number * 2);
        }
        return new Result(Result.NOT_EVEN);
    }

    /**
     * Wrapper Klasse fuer Rueckgabeobjekt.
     */
    class Result {

        static final int NOT_EVEN = -1;

        int number;

        Result(int number) {
            this.number = number;
        }

        boolean isEven() {
            return number != NOT_EVEN;
        }

        int getNumber() {
            return number;
        }
    }

}