package de.cherriz.training.java.generics;

public class DoubleCalculator implements Calculator<Double> {

    @Override
    public Double add(Double valA, Double valB) {
        return Double.valueOf(valA.doubleValue() + valB.doubleValue());
    }

    @Override
    public Double remove(Double valA, Double valB) {
        return Double.valueOf(valA.doubleValue() - valB.doubleValue());
    }

    @Override
    public Double multiply(Double valA, Double valB) {
        return Double.valueOf(valA.doubleValue() * valB.doubleValue());
    }

    @Override
    public Double divide(Double valA, Double valB) {
        return Double.valueOf(valA.doubleValue() / valB.doubleValue());
    }

}