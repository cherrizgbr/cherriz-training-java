package de.cherriz.training.java.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericFunctionShowCase {

    public <T> List<T> createTypeList(T item) {
        List<T> lst = new ArrayList<>();
        lst.add(item);
        return lst;
    }

    public <T extends Number> List<T> createTypeExtendsList(T item) {
        List<T> lst = new ArrayList<>();
        lst.add(item);
        return lst;
    }

    public <T> void copy(List<? super T> dest, List<? extends T> src) {
        for (T item : src) {
            dest.add(item);
        }
    }

}