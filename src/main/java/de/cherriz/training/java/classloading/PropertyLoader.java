package de.cherriz.training.java.classloading;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {

    public Properties readFile(String fileName) throws IOException {
        InputStream resource = this.getClass().getClassLoader().getResourceAsStream(fileName);
        Properties properties = new Properties();
        properties.load(resource);
        return properties;
    }

}