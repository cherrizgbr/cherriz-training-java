package de.cherriz.training.java.classloading;

public interface Merge {

    String merge(String a, String b);

}