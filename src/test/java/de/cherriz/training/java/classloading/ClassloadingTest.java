package de.cherriz.training.java.classloading;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Properties;

public class ClassloadingTest {

    @Test
    public void createInstance() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        ClassLoader classLoader = ClassloadingTest.class.getClassLoader();
        Class<?> clazz = classLoader.loadClass("java.lang.String");
        String s1 = new String();
        String s2 = (String) clazz.newInstance();

        Assert.assertEquals(s1, s2);
    }

    @Test
    public void readFile() throws IOException {
        PropertyLoader loader = new PropertyLoader();
        Properties properties = loader.readFile("test.properties");
        String valA = properties.getProperty("valA");

        Assert.assertEquals("Hallo", valA);
    }


    @Test
    public void loadClass() throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        File file = new File("src/main/resources/mergeimpl.jar");
        URL url = file.toURI().toURL();
        URLClassLoader child = new URLClassLoader(new URL[]{url}, this.getClass().getClassLoader());
        Class clazz = Class.forName("de.cherriz.training.java.classloading.MergeImpl", true, child);
        Merge merge = (Merge) clazz.newInstance();

        String result = merge.merge("a", "b");

        Assert.assertEquals("ab", result);
    }

}