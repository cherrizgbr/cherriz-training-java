package de.cherriz.training.java.reflection;

import de.cherriz.training.java.generics.Calculator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

public class ReflectionTest {

    private static ClassExaminator showCase;

    private static ObjectCreator creator;

    private static ObjectManipulator manipulator;

    private static ObjectManipulatorAdvanced manipulatorAdv;

    private static CalculatorCreator calcCreator;

    @BeforeClass
    public static void init() {
        showCase = new ClassExaminator();
        creator = new ObjectCreator();
        manipulator = new ObjectManipulator();
        manipulatorAdv = new ObjectManipulatorAdvanced();
        calcCreator = new CalculatorCreator();
    }

    @Test
    public void analyze() {
        Class<String> stringClass = String.class;
        showCase.listAttributes(stringClass);
        showCase.listFunctions(stringClass);
        showCase.listConstructors(stringClass);
    }

    @Test
    public void createSimple() {
        String object = creator.createSimpleObject(String.class);
        Assert.assertNotNull(object);
    }

    @Test
    public void createWithAttributes() {
        StringBuffer object = creator.createObjectWithAttributes(StringBuffer.class, "Test");
        Assert.assertNotNull(object);
        Assert.assertEquals("Test", object.toString());
    }

    @Test
    public void createFromString() {
        Object object = creator.createObjectWithAttributesFromString("java.lang.StringBuffer", "Test");
        Assert.assertNotNull(object);
        Assert.assertTrue(object instanceof StringBuffer);
        Assert.assertEquals("Test", object.toString());
    }

    @Test
    public void logCreation() {
        Fridge fridge = new Fridge(5);
        fridge.open();
        fridge.addItem(new Milk(1000, LocalDate.of(2012, Month.DECEMBER, 12)));
        fridge.addItem(new Cheese("Gouda", 5));
        fridge.addItem(new Apple("Topaz", "Bodensee"));

        System.out.println(fridge);
    }

    @Test
    public void manipulateObject() {
        Tresor tresor = new Tresor(1000);
        String code = manipulator.getFieldValue(tresor, "CODE");

        Assert.assertTrue(tresor.open(code));
        Assert.assertEquals(1000, tresor.getMoney());
    }

    @Test
    public void manipulateObjectAndSet() {
        Tresor tresor = new Tresor(1000);

        int inhaltAlt = manipulatorAdv.setFieldValue(tresor, "geld", 0);
        int inhaltNeu = manipulator.getFieldValue(tresor, "geld");

        Assert.assertEquals(1000, inhaltAlt);
        Assert.assertEquals(0, inhaltNeu);
    }

    @Test
    public void invoke() {
        Tresor tresor = new Tresor(1000);
        tresor.open("ABCD");
        int money = manipulator.invokeMethod(tresor, "getMoney");

        Assert.assertEquals(1000, money);
    }

    @Test
    public void initCalc() {
        Calculator<Double> calc = calcCreator.createCalculator();

        Assert.assertNotNull(calc);
        Assert.assertEquals(Double.valueOf(10), calc.add(Double.valueOf(6), Double.valueOf(4)));
    }

}