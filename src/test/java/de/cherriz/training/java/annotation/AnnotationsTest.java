package de.cherriz.training.java.annotation;

import net.sf.aspect4log.Log;
import org.junit.Test;

public class AnnotationsTest {

    @Test
    public void testLogAspect() {
        doSomething();
    }

    @Test
    public void testAnnotation() {
        methodA();
        methodB();
    }

    @TimeLog(level = Log.Level.ERROR)
    public String doSomething() {
        String str = "";
        for (int i = 0; i < 100; i++) {
            str += " ";
        }
        return str;
    }

    @Author(@Name(firstname = "Frederik", lastname = "Kirsch"))
    public void methodA() {
    }

    @Author(value = @Name(lastname = "Kirsch"), company = "abc GmbH")
    public void methodB() {
    }

    @PermissionCheck
    public void accessible() {
    }

    @PermissionCheck({"0001RECHTA"})
    public void oneRight() {
    }

    @PermissionCheck({"0001RECHTA", "0001RECHTB"})
    public void multipleRights() {
    }

}