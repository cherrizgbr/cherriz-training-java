package de.cherriz.training.java.generics;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GenericsTest {

    @Test
    public void beforeGenerics() {
        List oldList = new ArrayList();
        oldList.add("Test");
        oldList.add(Long.valueOf(1L));
        Object val1 = oldList.get(0);
        String strVal = (String) val1;
        Object val2 = oldList.get(1);
        Long longVal = (Long) val2;

        Assert.assertEquals("Test", strVal);
        Assert.assertEquals(Long.valueOf(1L), longVal);
    }

    @Test
    public void withGenerics() {
        List<String> strList = new ArrayList<>();
        strList.add("Test");
        String strVal = strList.get(0);
        Assert.assertEquals("Test", strVal);
    }

    @Test
    public void multipleResultTypes() {
        ResultShowCase resultShowCase = new ResultShowCase();
        ResultShowCase.Result doubled = resultShowCase.doubleIfEven(2);
        ResultShowCase.Result notDoubled = resultShowCase.doubleIfEven(3);

        Assert.assertTrue(doubled.isEven());
        Assert.assertEquals(4, doubled.getNumber());
        Assert.assertFalse(notDoubled.isEven());
    }

    @Test
    public void genericFunction(){
        GenericFunctionShowCase functionShowCase = new GenericFunctionShowCase();
        List<String> strList = functionShowCase.createTypeList("Test");
        List<Long> extendsList = functionShowCase.createTypeExtendsList(Long.valueOf(1L));

        List<Number> destLst = new ArrayList<>();
        List<Long> srcList = new ArrayList<>();
        srcList.add(Long.valueOf(1L));
        functionShowCase.copy(destLst, srcList);

        Assert.assertEquals("Test", strList.get(0));
        Assert.assertEquals(Long.valueOf(1L), extendsList.get(0));
        Assert.assertEquals(Long.valueOf(1L), destLst.get(0));
    }

}