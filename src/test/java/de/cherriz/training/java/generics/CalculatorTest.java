package de.cherriz.training.java.generics;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

    Double valA = Double.valueOf(5);

    Double valB = Double.valueOf(5);

    Calculator<Double> calc;

    @Before
    public void setUp() {
        calc = new DoubleCalculator();
    }

    @Test
    public void add() {
        Double resultExp = Double.valueOf(10L);
        Double result = calc.add(valA, valB);
        Assert.assertEquals(resultExp, result);
    }

    @Test
    public void remove() {
        Double resultExp = Double.valueOf(0L);
        Double result = calc.remove(valA, valB);
        Assert.assertEquals(resultExp, result);
    }


    @Test
    public void multiply() {
        Double resultExp = Double.valueOf(25L);
        Double result = calc.multiply(valA, valB);
        Assert.assertEquals(resultExp, result);
    }

    @Test
    public void divide() {
        Double resultExp = Double.valueOf(1L);
        Double result = calc.divide(valA, valB);
        Assert.assertEquals(resultExp, result);
    }

}