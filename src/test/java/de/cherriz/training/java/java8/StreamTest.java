package de.cherriz.training.java.java8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

public class StreamTest {

    private StreamShowCase streamShowCase;

    private List<Person> personen;

    @Before
    public void setUp() {
        streamShowCase = new StreamShowCase();

        personen = new LinkedList<>();
        personen.add(new Person("Hans", "Mayer", true, 10, "12345"));
        personen.add(new Person("Rudi", "Ruppig", true, 20, "45678"));
        personen.add(new Person("Detlef", "Bauer", true, 30, "12345"));
        personen.add(new Person("Heidi", "Kurz", false, 40, "45678"));
        personen.add(new Person("Susi", "Menge", false, 50, "12345"));
        personen.add(new Person("Verena", "Meinl", false, 60, "55442"));
    }

    @Test
    public void avgAge() {
        OptionalDouble avgAge = streamShowCase.avgAge(personen);

        Assert.assertTrue(avgAge.isPresent());
        Assert.assertEquals(35, avgAge.getAsDouble(), 1);
    }

    @Test
    public void poplation() {
        long population = streamShowCase.population(personen, "12345");

        Assert.assertEquals(3, population);
    }

    @Test
    public void guestList() {
        Optional<String> liste = streamShowCase.createGaesteliste(personen, "12345");

        Assert.assertTrue(liste.isPresent());
        Assert.assertEquals("Herr Detlef Bauer\n" +
                "Herr Hans Mayer\n" +
                "Frau Susi Menge", liste.get());
    }

    @Test
    public void countLetters(){
        List<String> woerter = new LinkedList<>();
        woerter.add("Fast");
        woerter.add("alle");
        woerter.add("Buchstaben");
        woerter.add("werden");
        woerter.add("gezählt");

        LetterCounter counter = new LetterCounter();
        Optional<Integer> anz = counter.count(woerter);
        Assert.assertTrue(anz.isPresent());
        Assert.assertEquals(24,anz.get().intValue());
    }

}