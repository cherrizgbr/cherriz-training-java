package de.cherriz.training.java.java8;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class IterationTest {

    private static IterationShowCase showCase;

    private static List<String> strLst;

    @BeforeClass
    public static void init() {
        showCase = new IterationShowCase();
        strLst = new LinkedList<>();
        strLst.add("A");
        strLst.add("B");
        strLst.add("C");
        strLst.add("D");
        strLst.add("E");
        strLst.add("F");
    }

    @Test
    public void iteratePriorJava5() {
        showCase.iteratePriorJava5(strLst);
    }

    @Test
    public void iterateJava5() {
        showCase.iteratePriorJava5(strLst);
    }

    @Test
    public void IterateJava8() {
        showCase.iteratePriorJava5(strLst);
    }

}