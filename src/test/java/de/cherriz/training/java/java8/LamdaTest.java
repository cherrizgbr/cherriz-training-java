package de.cherriz.training.java.java8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LamdaTest {

    private static BusinessCalculator calc;

    @Before
    public void init() {
        calc = new BusinessCalculator();
    }

    @Test
    public void priceWithTaxes() {
        calc.add(49.99, 5);

        //Mehrwehrtssteuer
        calc.applyRule(p -> p * 1.19);

        double price = calc.getPrice();
        Assert.assertEquals(297.44, price, 0.0);
    }

    @Test
    public void multipleProdWithTaxes() {
        calc.add(49.99, 5);
        calc.add(14.95, 5);

        //Mehrwehrtssteuer
        calc.applyRule(p -> p * 1.19);

        double price = calc.getPrice();
        Assert.assertEquals(386.39, price, 0.0);
    }

    @Test
    public void multipleProdWithMultipleRules() {
        calc.add(49.99, 5);
        calc.add(14.95, 5);

        //Mehrwehrtssteuer
        calc.applyRule(p -> p * 1.19);
        //Skonto
        calc.applyRule(p -> p * 0.98);
        //Nach Dollar
        calc.applyRule(p -> p * 1.08835);

        double price = calc.getPrice();
        Assert.assertEquals(412.12, price, 0.0);
    }

    @Test
    public void calc() {
        LambdaCalc calc = new LambdaCalc();
        int result = calc.calculate(2, 2, (a, b) -> a + b);

        Assert.assertEquals(4, result);
    }


}